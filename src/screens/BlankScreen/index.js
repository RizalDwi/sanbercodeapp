import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import Asynstorage from '@react-native-community/async-storage'
import auth from '@react-native-firebase/auth'


export default function index({ navigation }) {

    useEffect(() => {
        Asynstorage.getItem('intro', (error, result) =>{
            console.log(Asynstorage.getItem('intro'))
            if (result){
                auth().onAuthStateChanged(function(user) {
                    if (user) {
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Home' }]
                        })
                    } else {
                        navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }]
                        })
                    }
                })

                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }]
                })
            } else {
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Intro' }]
                })
            }
          })
    })

    return (
        <View>
            
        </View>
    )
}
