import React,{useState, Component} from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, Button, ScrollView } from 'react-native'

export default function TodoList() {
  const [listNote, setListNote] = useState([]);
  const [textInput, setTextInput] = useState({getCurrentDate});
  const [tanggal, setTanggal] = useState('')
  const getCurrentDate = () => {

    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    setTanggal(date.toString()+'/'+month.toString()+'/'+year.toString())
  }

  const Note = (props) => {
  
    return(
      <View style={{marginVertical: 5 ,borderColor: 'gray', borderWidth: 1, width: '100%', height: 100, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 15}}>
          <View>
          <Text style={{fontSize: 16}}>{props.waktu}</Text>
          <Text style={{fontSize: 16}}>{props.data}</Text>
        </View>
        <TouchableOpacity onPress={() => deleteItem(props.id)}>
          <Image source={require('./../../assets/icons/sampah.png')} style={{width: 40, height: 40}} />
        </TouchableOpacity>
      </View>
    )
  }

  const [refreshing, setRefreshing] = React.useState(false);

  const getInput = (input) => {
    setTextInput(input)
  }
  const tambah = () =>{
    //setTextInput(input)
    getCurrentDate()
    setListNote(listNote => [...listNote, textInput])
  }

  const deleteItem = (id) => {
      var index = listNote.indexOf(id)
      if (index > -1) {
        listNote.splice(index, 1)
        setRefreshing(true) 
        setTimeout(() => {
          setRefreshing(false);
        }, 200)
      }
      
  }
  
  return(
    <ScrollView style={{flex: 1, paddingTop: 20, paddingHorizontal: 10}}>
            <View>
              <Text>Masukan Todolist</Text>
              <View style={{flexDirection: 'row', marginVertical: 10}}>
                <TextInput placeholder='Input here' onChangeText = {getInput} style={{borderColor: 'black', borderWidth: 1, flex: 1, marginRight: 5, paddingHorizontal: 10}} />
                <TouchableOpacity onPress={() => tambah()}>
                  <Image source={require('./../../assets/icons/add.png')} style={{width: 50, height: 50}} />
                </TouchableOpacity>
              </View>
              <View style={{marginTop: 10}}>
              {listNote.map((item, index) => <Note data={item} waktu = {tanggal} key={index} id={item} /> )}
              </View>
            </View>
            
    </ScrollView>
  )
}
