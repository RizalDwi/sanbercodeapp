import React, {useContext} from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, TextInput, FlatList } from 'react-native'
import { RootContext } from './index'

export default function ToDolist() {

    const state = useContext(RootContext)

    const renderItem = ({ item, index }) => {
        return (
                <View style={{marginVertical: 5 ,borderColor: 'gray', borderWidth: 1, width: '100%', height: 100, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 15}}>
                    <View>
                    <Text style={{fontSize: 16}}>{item.date}</Text>
                    <Text style={{fontSize: 16}}>{item.title}</Text>
                    </View>
                    <TouchableOpacity onPress={() => state.deleteItem(index)}>
                    <Image source={require('./../../assets/icons/sampah.png')} style={{width: 40, height: 40}} />
                    </TouchableOpacity>
                   
                </View>
        )
    }

    return (
        <ScrollView style={{flex: 1, paddingTop: 20, paddingHorizontal: 10}}>
            <View>
              <Text>Masukan Todolist</Text>
              <View style={{flexDirection: 'row', marginVertical: 10}}>
                <TextInput value={state.input} placeholder='Input here' onChangeText = {(value) => state.handleChangeInput(value)} style={{borderColor: 'black', borderWidth: 1, flex: 1, marginRight: 5, paddingHorizontal: 10}} />
                <TouchableOpacity onPress={() => state.addTodo()}>
                  <Image source={require('./../../assets/icons/add.png')} style={{width: 50, height: 50}} />
                </TouchableOpacity>
              </View>
              <View style={{marginTop: 10}}>
                <FlatList
                    data={state.todos}
                    renderItem={renderItem}
                />
              </View>
            </View>
            
    </ScrollView>
    )
}
